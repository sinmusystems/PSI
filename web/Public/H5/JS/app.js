var app = new Framework7({
			root : '#app',
			id : 'com.gitee.crm8000.psi',
			name : 'PSI',
			theme : 'auto',
			view : {
				pushState : true,
				xhrCache : false
			},
			data : function() {
				return {
					PSI : {
						productionName : productionName,
						demoLoginInfo : demoLoginInfo,
						baseURI : baseURI,
						userIsLoggedIn : userIsLoggedIn,
						version: "PSI-H5 2018 build201805232250"
					}
				};
			},
			methods : {},
			routes : routesPSI,
			dialog : {
				buttonOk : "确定",
				buttonCancel : "取消",
				title : productionName
			},
			calendar : {
				monthNames : ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月",
						"九月", "十月", "十一月", "十二月"],
				dayNamesShort : ["日", "一", "二", "三", "四", "五", "六"],
				firstDay : 0,
				dateFormat : "yyyy-mm-dd"
			},
			autocomplete : {
				searchbarPlaceholder : "搜索...",
				notFoundText : "没有数据"
			}
		});

var mainView = app.views.create('.view-main', {
			url : '/'
		});